import os
from argparse import ArgumentParser
from functools import partial
from multiprocessing.pool import ThreadPool
from operator import is_not

from dateutil.parser import parse
from datetime import datetime
from jinja2 import Environment, FileSystemLoader
from bs4 import BeautifulSoup
import requests

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

j2_env = Environment(loader=FileSystemLoader(THIS_DIR),
                     trim_blocks=True)
j2_env.get_template('./template/frapant.jinja2')

open("/tmp/sources.html", "w")


def print_html_doc(sources):
    j2_env.get_template('template/frapant.jinja2').stream(
        sources=sources
    ).dump("/tmp/sources.html")


def parse_args():
    parser = ArgumentParser(description="cite sources to html")
    parser.add_argument('urls', metavar='url', type=str, nargs='+', help="the urls that need to be processed")
    parser.add_argument('--file', dest='infile', action='store_const', const=True, default=False,
                        help="get the urls from a file (default No)")

    return parser.parse_args()


def readfromfile(fileloc):
    with open(fileloc) as f:
        for line in f.readlines():
            yield line[:-1]


def procesurls(_args):
    _linklist = []
    if _args.infile:
        assert len(_args.urls) == 1
        _linklist = readfromfile(_args.urls[0])
    else:
        assert len(_args.urls) > 0
        _linklist = _args.urls
    return _linklist


def workerfunc(url):
    r = requests.get(url)
    if r.status_code != 200:
        return
    data = r.text
    soup = BeautifulSoup(data, features='html.parser')
    author = soup.find("meta", property="og:site_name")["content"] \
        if soup.find("meta", property="og:site_name") is not None else url.split(".")[1][0].capitalize() + \
                                                                       url.split(".")[1][1:]
    lastmodified = r.headers["Last-Modified"] if "last-modified" in r.headers else "z.d."
    if lastmodified != "z.d.":
        lastmodified = parse(lastmodified, fuzzy=True).strftime("%d.%m.%Y")
    source = {"title": str(soup.title.text), "author": author,
              "lastupdated": lastmodified,
              "viewdate": datetime.today().strftime("%d.%m.%Y"),
              "organisation": author, "link": url}
    return source


def getsources(_urls):
    pool = ThreadPool(5)
    results = pool.map(workerfunc, _urls)
    results = filter(partial(is_not, None), results)
    return sorted(results, key=lambda k: k['organisation'].lower())


if __name__ == "__main__":
    args = parse_args()
    urls = procesurls(args)
    sources = getsources(urls)
    print_html_doc(sources)

