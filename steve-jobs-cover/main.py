from math import sqrt

import cv2
import sys

counter = 0


def image_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation=inter)

    # return the resized image
    return resized


cascPath = sys.argv[1]
faceCascade = cv2.CascadeClassifier(cascPath)

video_capture = cv2.VideoCapture(0)
steve = cv2.imread("data/cover-steve.jpg")
steve_grey = cv2.cvtColor(steve, cv2.COLOR_BGR2GRAY)
eyes_steve = faceCascade.detectMultiScale(steve_grey,
                                          scaleFactor=1.1,
                                          minNeighbors=5,
                                          minSize=(30, 30),
                                          flags=cv2.CASCADE_SCALE_IMAGE)

assert len(eyes_steve) == 2
x, y, _, _2 = eyes_steve[0]
x2, y2, w, _ = eyes_steve[1]
placement_offset_eye = (-x, -y, steve.shape[1] - w, -y2)
eye1 = eyes_steve[0]
eye2 = eyes_steve[1]
distance_between_eyes_steve = round(sqrt((eye1[0] - eye2[0]) ** 2 + (eye1[1] - eye2[1]) ** 2))
print(distance_between_eyes_steve)
while counter < 150:
    # Capture frame-by-frame
    ret, frame = video_capture.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    eyes = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    for (x, y, w, h) in eyes:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

    if len(eyes) == 2:
        eye1 = eyes[0]
        eye2 = eyes[1]
        distance_between_eyes = int(sqrt((eye1[0] - eye2[0]) ** 2 + (eye1[1] - eye2[1]) ** 2))
        scalefactor = distance_between_eyes / distance_between_eyes_steve
        copy_steve = cv2.resize(steve, None, fx=scalefactor, fy=scalefactor)
        print(scalefactor)
        nx = int(eye1[0] + placement_offset_eye[0] * scalefactor)
        ny = int(eye1[1] + placement_offset_eye[1] * scalefactor)
        print(copy_steve.shape)
        if 0 <= ny + copy_steve.shape[0] <= frame.shape[0] and 0 <= nx + copy_steve.shape[1] <= frame.shape[1]:
            print("check1")
            if 0 <= ny and 0 <= nx:
                frame[ny:ny + copy_steve.shape[0], nx:nx + copy_steve.shape[1]] = copy_steve
                if counter > 20:
                    cv2.imwrite("data/out/out_{}.jpg".format(counter), frame)
                counter += 1

    # Display the resulting frame
    cv2.imshow('Video', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
