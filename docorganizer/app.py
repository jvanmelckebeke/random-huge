from flask import Flask, render_template, request, redirect, url_for

import helpers

app = Flask(__name__)

local_cache = {"unsorted": []}
storage = dict()


@app.route('/')
@app.route('/dashboard')
def home():
    return render_template('dashboard.jinja2', unsorted=local_cache["unsorted"])


@app.route('/upload/', methods=["GET", "POST"])
def upload_bulk():
    if request.method == "GET":
        return render_template("upload.jinja2")
    files = request.files.getlist("files[]")
    filenames = helpers.process_files(files)
    local_cache["unsorted"].extend(filenames)
    for file in filenames:
        storage[file["guid"]] = file
    return redirect(url_for("home"))


@app.route('/update/<id>', methods=["GET", "POST"])
def update(id):
    if request.method == "GET":
        return render_template("update.jinja2", file=storage[id])


if __name__ == '__main__':
    app.run()
