import uuid


def process_files(files):
    ret = []
    for f in files:
        print(f.filename)
        guid = str(uuid.uuid4())
        f.save("/tmp/%s" % guid)
        ret.append({"guid": guid, "name": f.filename})

    return ret
