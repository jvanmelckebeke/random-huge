from ulauncher.api.client.Extension import Extension
from ulauncher.api.client.EventListener import EventListener
from ulauncher.api.shared.event import KeywordQueryEvent, ItemEnterEvent
from ulauncher.api.shared.item.ExtensionResultItem import ExtensionResultItem
from ulauncher.api.shared.action.RenderResultListAction import RenderResultListAction
from ulauncher.api.shared.action.HideWindowAction import HideWindowAction

from urllib import urlencode
import urllib2

url = "https://songbpm.com/api/search"


class BPMExtension(Extension):

    def __init__(self):
        super(BPMExtension, self).__init__()
        self.subscribe(KeywordQueryEvent, KeywordQueryEventListener())


class KeywordQueryEventListener(EventListener):

    def on_event(self, event, extension):
        arg = event.get_argument()
        post_field = urlencode({"query": str(arg)})
        request = urllib2.Request(url, post_field)
        json = urllib2.urlopen(request).read().decode()
        items = []
        for item in json["search"]["songs"]:
            items.append(ExtensionResultItem(icon='images/icon.png',
                                             name='%s: %s' % (item["title"], item["tempo"]),
                                             description='Item description %s' % item["artist"],
                                             on_enter=HideWindowAction()))

        return RenderResultListAction(items)


if __name__ == '__main__':
    BPMExtension().run()
